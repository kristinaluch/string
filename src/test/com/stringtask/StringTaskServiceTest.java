package test.com.stringtask;

import main.com.stringtask.services.StringTaskService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class StringTaskServiceTest {
    StringTaskService cut = new StringTaskService();

    @Test
    void getEnglishABCTest(){
        String expected = "a b c d e f g h i j k l m n o p q r s t u v w x y z ";
        String actual = cut.getEnglishABC();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getReverseEnglishABCTest(){
        String expected = "z y x w v u t s r q p o n m l k j i h g f e d c b a ";
        String actual = cut.getReverseEnglishABC();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getRussianABCTest(){
        String expected = "� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ";
        String actual = cut.getRussianABC();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getNumberTest(){
        String expected = "0 1 2 3 4 5 6 7 8 9 ";
        String actual = cut.getNumber();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getASCIITest(){
        String expected = "! \" # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \\ ] ^ _ ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~ ";
        String actual = cut.getASCII();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] changeIntToStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments("5", 5),
                Arguments.arguments("0", 0),
                Arguments.arguments("-570", -570),
        };
    }


    @ParameterizedTest
    @MethodSource("changeIntToStringTestArgs")
    void changeIntToStringTest(String expected, int number){
        String actual = cut.changeIntToString(number);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] changeDoubleToStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments("0.35", 0.35),
                Arguments.arguments("-7.3", -7.3),
                Arguments.arguments("999.000000666", 999.000000666),
        };
    }


    @ParameterizedTest
    @MethodSource("changeDoubleToStringTestArgs")
    void changeDoubleToStringTest(String expected, double number){
        String actual = cut.changeDoubleToString(number);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] changeStringToIntTestArgs(){
        return new Arguments[]{
                Arguments.arguments(89, "89"),
                Arguments.arguments(-289, "-289"),
                Arguments.arguments(0, "0")
        };
    }


    @ParameterizedTest
    @MethodSource("changeStringToIntTestArgs")
    void changeStringToIntTest(int expected, String str){
        int actual = cut.changeStringToInt(str);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] changeStringToDoubleTestArgs(){
        return new Arguments[]{
                Arguments.arguments(0.35, "0.35"),
                Arguments.arguments(-3.333, "-3.333"),
                Arguments.arguments(999.000000666, "999.000000666"),
        };
    }


    @ParameterizedTest
    @MethodSource("changeStringToDoubleTestArgs")
    void changeStringToDoubleTest(double expected, String str) {
        double actual = cut.changeStringToDouble(str);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getShortWordTestArgs(){
        return new Arguments[]{
                Arguments.arguments("My", "My,name !iss ?Alice"),
                Arguments.arguments("���", "����������!\n" +
                        "����, ���� ������ �������� �\n" +
                        "������ � ��� ����-������ �����?")
        };
    }


    @ParameterizedTest
    @MethodSource("getShortWordTestArgs")
    void getShortWordTest(String expected, String line){
        String actual = cut.getShortWord(line);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getThreeDollarsEndWordTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"perfect", "de$$$", "beautiful", "bl$$$", "cr$$$"}, new String[]{"perfect", "death", "beautiful", "blood", "crown"}, 5),
                Arguments.arguments(null, new String[]{"It", "is", "java,", "it", "is", "Sparta!"}, 2),
                Arguments.arguments(new String[]{"$$$", "I", "$$$", "like", "$$$", "it", "$$$"}, new String[]{"not", "I", "cat", "like", "rat", "it", "dog"} , 3)
        };
    }

    @ParameterizedTest
    @MethodSource("getThreeDollarsEndWordTestArgs")
    void getThreeDollarsEndWord(String[] expected, String[] arrayWord, int wordLength){
        String[] actual = cut.getThreeDollarsEndWord(arrayWord, wordLength);
        Assertions.assertArrayEquals(expected,actual);
    }

    static Arguments[] addSpaceTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Animal, I. have; . become", "Animal,I. have;.become"),
                Arguments.arguments("Three: Days , Grace", "Three:Days , Grace"),
        };
    }

    @ParameterizedTest
    @MethodSource("addSpaceTestArgs")
    void addSpace(String expected, String line){
        String actual = cut.addSpace(line);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] leaveOneCopyOfSymbolTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Tula", "Tulula"),
                Arguments.arguments("1 23", "1 2 3 2"),
        };
    }

    @ParameterizedTest
    @MethodSource("leaveOneCopyOfSymbolTestArgs")
    void leaveOneCopyOfSymbol(String expected, String line){
        String actual = cut.leaveOneCopyOfSymbol(line);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] countWordsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3, "Three.Days!Grace"),
                Arguments.arguments(8, "    1,2.Three\n4?5!6:7\"8"),
        };
    }

    @ParameterizedTest
    @MethodSource("countWordsTestArgs")
    void countWords(int expected, String line){
        int actual = cut.countWords(line);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] deletePartTestArgs(){
        return new Arguments[]{
                Arguments.arguments("1236789", "123456789", 4, 2),
                Arguments.arguments("123456","123456789", 7, 3),
                Arguments.arguments("incorrect number!", "123456789", 5, 6),
                Arguments.arguments("", "123456789", 1, 9)
        };
    }

    @ParameterizedTest
    @MethodSource("deletePartTestArgs")
    void deletePart(String expected, String line, int position, int lengthPart){
        String actual = cut.deletePart(line, position, lengthPart);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] reverseLineTestArgs(){
        return new Arguments[]{
                Arguments.arguments("54321", "12345"),
                Arguments.arguments("Think about it", "ti tuoba knihT"),
        };
    }

    @ParameterizedTest
    @MethodSource("reverseLineTestArgs")
    void reverseLine(String expected, String line){
        String actual = cut.reverseLine(line);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] deleteEndWordTestArgs(){
        return new Arguments[]{
                Arguments.arguments("I don't ", "I don't know"),
                Arguments.arguments("Delete last !", "Delete last word!"),
        };
    }

    @ParameterizedTest
    @MethodSource("deleteEndWordTestArgs")
    void deleteEndWordTest(String expected, String line){
        String actual = cut.deleteEndWord(line);
        Assertions.assertEquals(expected,actual);
    }



}
