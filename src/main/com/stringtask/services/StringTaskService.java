package main.com.stringtask.services;

import java.util.Arrays;
import java.util.regex.Pattern;

public class StringTaskService {

    public String getEnglishABC(){
        char[] chars = new char[52];
        int nArr = 0;
        for (char ch = 'a'; ch <= 'z'; ch++){
            chars[nArr] = ch;
            ++nArr;
            chars[nArr] = ' ';
            ++nArr;
        }
        String englishABC = new String(chars);
        System.out.println(englishABC);
        return englishABC;
    }

    public String getReverseEnglishABC(){
        char[] chars = new char[52];
        int nArr = 0;
        for (char ch = 'z'; ch >= 'a'; ch--){
            chars[nArr] = ch;
            ++nArr;
            chars[nArr] = ' ';
            ++nArr;
        }
        String englishReverseABC = new String(chars);
        System.out.println(englishReverseABC);
        return englishReverseABC;
    }

    public String getRussianABC(){
        char[] chars = new char[66];
        int nArr = 0;
        for (char ch = '�'; ch <= '�'; ch++){
            chars[nArr] = ch;
            ++nArr;
            chars[nArr] = ' ';
            if (ch == '�'){
                ++nArr;
                chars[nArr] = '�';
                ++nArr;
                chars[nArr] = ' ';
            }
            ++nArr;
        }
        String englishABC = new String(chars);
        System.out.println(englishABC);
        return englishABC;
    }

    public String getNumber(){
        String result ="";
        for (int number = 9; number >=0; number--) {
            result = number+" "+result;
        }
        System.out.println(result);
        return result;
    }

    public String getASCII(){
        char[] chars = new char[188];
        int nArr = 0;
        for (char ch = 33; ch <= 126; ch++){
            chars[nArr] = ch;
            ++nArr;
            chars[nArr] = ' ';
            ++nArr;
        }
        String result = new String(chars);
        System.out.println(result);
        return result;
    }

    public String changeIntToString(int number){
        String str = ""+number;
        return str;
    }

    public String changeDoubleToString(double number){
        String str = ""+number;
        return str;
    }

     public int changeStringToInt(String str){
        int number = Integer.parseInt(str);
        return number;
     }

     public double changeStringToDouble(String str) {
        double number = Double.parseDouble(str);
         return number;
     }

     public String getShortWord(String line){
        String[] arrayLine;
        String[] regex = {",", "�", "\n","\"","\\", ".", ";", ":", "?", "/", "!", "  "};
         for (int r = 0; r < regex.length; r++) {
             line = line.replace(regex[r], " ");
         }
         line = line.replace("  ", " ");
        arrayLine = line.split(" ");
        int min = arrayLine[0].length();
        String minWord = arrayLine[0];
         for (int word = 1; word < arrayLine.length; word++) {
             if(arrayLine[word].length()<min){
                 min = arrayLine[word].length();
                 minWord = arrayLine[word];
             }
         }
        return minWord;
     }

     public String[] getThreeDollarsEndWord(String[] words, int wordLength){
        if (wordLength<3){
            System.out.println("������������ ����� �����!");
            return null;
        }
        int cut = wordLength - 3;

         for (int word = 0; word < words.length; word++) {
             if(words[word].length()==wordLength){
                 words[word] = words[word].substring(0, cut) + "$$$";
             }
         }
         return words;
     }

     public String addSpace(String line){
         String[] regex = {",", "�", "\n","\"","\\", ".", ";", ":", "?", "!"};
         for (int r = 0; r < regex.length; r++) {
             if (line.contains(regex[r])){
                line = line.replace(regex[r], (regex[r] +" "));
             }
         }
         line = line.replace("  ", " ");
        return line;
     }

     public String leaveOneCopyOfSymbol(String line){
        char[] chars = line.toCharArray();
        int[] count = new int[chars.length];
         for (int i = 0; i<chars.length; i++) {
             if (chars[i]=='\0'){
                 continue;
             }
             for (int j = 0; j<chars.length; j++){
                 if(chars[i]==chars[j]){
                     count[i]++;
                 }
                 if(count[i]>1){
                     chars[j]='\0';
                     break;
                 }
             }
         }
         line = new String(chars);
         line = line.replace("\0", "");
        return line;


     }
     public int countWords(String line){
        String[] words = line.split(" |,|\n|!|:|;|\"|\\.|\\?");
        int count = words.length;
         for (int i = 0; i < words.length; i++) {
             if(words[i].isBlank()){
              count--;
             }
         }
        return count;
     }

     public String deletePart(String line, int position, int lengthPart){
        int indBegin = position-1;
        int indEnd = indBegin+lengthPart;
        if(indEnd>line.length()){
            return "incorrect number!";
        }
        String del = line.substring(indBegin, indEnd);
        line = line.replace(del, "");
        return line;
     }

     public String reverseLine(String line){
        char[] chars = line.toCharArray();
        char change;
        int end = chars.length-1;
         for (int i = 0; i < chars.length/2; i++) {
             change = chars[i];
             chars[i] = chars[end-i];
             chars[end-i] = change;
         }
        String revers = new String(chars);
        return revers;
     }

     public String deleteEndWord(String line){
         String[] words = line.split(" |,|\n|!|:|;|\"|\\.|\\?");
         int numOfDelWord = words.length-1;
         String deleteWord = words[numOfDelWord];
         line = line.replace(deleteWord, "");
         return line;
     }
}
